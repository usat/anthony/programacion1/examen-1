/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  silviopd
 * Created: Jan 23, 2019
 */

select * from fn_actualizarUsuario(1,'cordova','diaz','christian','','','','1234')

create or replace function fn_actualizarUsuario(
	p_codigo int,
	p_paterno varchar,
	p_materno varchar,
	p_nombres varchar,
	p_direccion varchar,
	p_telefono varchar,
	p_correo varchar,
	p_pass varchar
) returns table(r_login varchar) as 
$$
Declare

Begin

    UPDATE usuario
	   SET 	paterno=p_paterno, 
		materno=p_materno, 
		nombres=p_nombres, 
		direccion=p_direccion, 
		telefono=p_telefono, 
		correo=p_correo, 
		login=((SELECT LEFT(p_nombres,1) )||p_paterno)::varchar, 
		pass=(SELECT MD5(p_pass))
	 WHERE codigo=p_codigo;


     return query 
     select ((SELECT LEFT(p_nombres,1) )||p_paterno)::varchar;
end;
$$language 'plpgsql';