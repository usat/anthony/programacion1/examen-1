select fn_crearUsuario('peña','diaz','jkhkjhkjhk','','','','123');

select * from usuario;

create or replace function fn_crearUsuario(
    p_paterno varchar,
    p_materno varchar,
    p_nombres varchar,
    p_direccion varchar,
    p_telefono varchar,
    p_correo varchar,
    p_pass varchar
) returns table(r_login varchar) as 
$$
Declare

Begin

    INSERT INTO usuario( 
        paterno, 
        materno, 
        nombres, 
        direccion, 
        telefono, 
        correo, 
        login, 
        pass) 
    VALUES ( 
        p_paterno, 
        p_materno, 
        p_nombres, 
        p_direccion, 
        p_telefono, 
        p_correo, 
        ((SELECT LEFT(p_nombres,1) )||p_paterno)::varchar, 
        (SELECT MD5(p_pass))::char(32)
     );

     return query 
     select ((SELECT LEFT(p_nombres,1) )||p_paterno)::varchar;
end;
$$language 'plpgsql';