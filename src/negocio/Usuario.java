/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author silviopd
 */
public class Usuario extends conexion {

    private int codigo;
    private String apellidopaterno, apellidomaterno, nombres, direccion, telefono, correo, login, pass;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getApellidopaterno() {
        return apellidopaterno;
    }

    public void setApellidopaterno(String apellidopaterno) {
        this.apellidopaterno = apellidopaterno;
    }

    public String getApellidomaterno() {
        return apellidomaterno;
    }

    public void setApellidomaterno(String apellidomaterno) {
        this.apellidomaterno = apellidomaterno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean agregar() throws Exception {

        try {
            Connection con = this.abrirConexion();
            con.setAutoCommit(false);

            String sql = "select * from fn_crearUsuario(?,?,?,?,?,?,?)";

            PreparedStatement sentencia = con.prepareStatement(sql);
            
            sentencia.setString(1, this.getApellidopaterno());
            sentencia.setString(2, this.getApellidomaterno());
            sentencia.setString(3, this.getNombres());
            sentencia.setString(4, this.getDireccion());
            sentencia.setString(5, this.getTelefono());
            sentencia.setString(6, this.getCorreo());
            sentencia.setString(7, this.getPass());

            this.ejecutarSQLSelectSP2(sentencia);

            con.commit();
            con.close();
            
            return true;
       
        } catch (Exception e) {
            throw new Exception(e);
        }

    }
    
    public ResultSet listar() throws Exception{
        String sql = "select * from usuario";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        
        ResultSet resultado = this.ejecutarSQLSelectSP(sentencia);
        return resultado;
    }

    public ResultSet leerDatos(int codigo) throws Exception{
        
        String sql = "select * from usuario where codigo=?";
        
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, codigo);
        
        ResultSet resultado = this.ejecutarSQLSelectSP(sentencia);
        
        return resultado;
    }

}
