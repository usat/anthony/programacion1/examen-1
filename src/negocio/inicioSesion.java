package negocio;

import datos.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
public class inicioSesion extends conexion{
    
    private String usuario,password;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public int iniciarSesionSP() throws Exception{
        
        String sql= "select * from fn_inicioSesion(?, ?)";
        
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setString(1, this.getUsuario());
        sentencia.setString(2, this.getPassword());
        
        ResultSet resultado = this.ejecutarSQLSelectSP(sentencia);
        
        if (resultado.next()){ //Encontró registros
            return 1;
        }
        
        return 0;
    }

}
