/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import javax.swing.UIManager;
import presentacion.frmInicioSesion;

/**
 *
 * @author silviopd
 */
public class inicio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            new frmInicioSesion().setVisible(true);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}
