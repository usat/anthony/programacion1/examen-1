/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.ResultSet;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author silviopd
 */
public class Funciones {
    
    public static void llenarTabla(JTable tabla, ResultSet resultado) throws Exception{
        DefaultTableModel modelo = new DefaultTableModel();
        int nroColumnas = resultado.getMetaData().getColumnCount();
        
        for (int i=0; i<nroColumnas; i++){
            modelo.addColumn(resultado.getMetaData().getColumnLabel(i+1).toUpperCase());
        }
        
        Object datos[] = new Object[nroColumnas];
        while (resultado.next()){
            for(int i=0; i<nroColumnas;i++){
                datos[i] = resultado.getObject(i+1);
            }
            modelo.addRow(datos);
        }
        tabla.setModel(modelo);
        
        /*INMOVILIZAR LAS COLUMAS DE LA TABLA*/
        tabla.getTableHeader().setReorderingAllowed(false);
        /*INMOVILIZAR LAS COLUMAS DE LA TABLA*/
        
    }
    
    public static void llenarTabla(JTable tabla, ResultSet resultado, int[] anchoColumna, String[] alineacionColumna) throws Exception{
        llenarTabla(tabla, resultado);
        
        tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tabla.setAutoCreateRowSorter(true);
        
        for (int i=0; i<anchoColumna.length; i++){
            DefaultTableCellRenderer alineacion = new DefaultTableCellRenderer();
            if (alineacionColumna[i].equalsIgnoreCase("C") ){
                alineacion.setHorizontalAlignment(SwingConstants.CENTER);
            }else if (alineacionColumna[i].equalsIgnoreCase("I") ){
                alineacion.setHorizontalAlignment(SwingConstants.LEFT);
            }else{
                alineacion.setHorizontalAlignment(SwingConstants.RIGHT);
            }
            tabla.getColumnModel().getColumn(i).setCellRenderer(alineacion);
            tabla.getColumnModel().getColumn(i).setPreferredWidth(anchoColumna[i]);
        }
    }
    
}
